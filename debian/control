Source: gtkglextmm
Maintainer: Gert Wollny <gw.fossdev@gmail.com>
Section: devel
Priority: optional
Build-Depends: autotools-dev,
               debhelper (>= 10~),
               libglibmm-2.4-dev (>= 2.44.0-2),
               libgtkglext1-dev,
               libgtkmm-2.4-dev (>= 1:2.24.4-2),
               mm-common
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/med-team/gtkglextmm
Vcs-Git: https://salsa.debian.org/med-team/gtkglextmm.git
Homepage: http://www.k-3d.org/gtkglext/

Package: libgtkglextmm-x11-1.2-0v5
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Conflicts: libgtkglextmm-x11-1.2-0
Replaces: libgtkglextmm-x11-1.2-0
Description: C++ bindings for GtkGLExt (Shared libraries)
 GtkGLExtmm is a C++ wrapper for GtkGLExt. C++ programmers can use it to
 write GTK+-based OpenGL applications using Gtkmm 2.
 .
 This package contains the shared libraries.

Package: libgtkglextmm-x11-1.2-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libgtkglextmm-x11-1.2-0v5 (= ${binary:Version}),
         libgtkglext1-dev,
         libglibmm-2.4-dev,
         libgtkmm-2.4-dev,
         libpangomm-1.4-dev
Pre-Depends: ${misc:Pre-Depends}
Description: C++ bindings for GtkGLExt (Development files)
 GtkGLExtmm is a C++ wrapper for GtkGLExt. C++ programmers can use it to
 write GTK+-based OpenGL applications using Gtkmm 2.
 .
 This package contains the development files.

Package: libgtkglextmm-x11-1.2-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: libgtkglextmm-x11-1.2-dev (= ${binary:Version})
Description: C++ bindings for GtkGLExt (Documentation)
 GtkGLExtmm is a C++ wrapper for GtkGLExt. C++ programmers can use it to
 write GTK+-based OpenGL applications using Gtkmm 2.
 .
 This package contains documentation and examples.
