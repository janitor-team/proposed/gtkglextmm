;
; NSIS script for gtkglextmm Win32 binary installer.
;
; written by Naofumi Yasufuku <naofumi@users.sourceforge.net>
;
; about NSIS, see http://nsis.sourceforge.net/
;

!define GTKGLEXTMM_VERSION         1.2.0
!define GTKGLEXTMM_API_VERSION     1.2
!define GTKGLEXTMM_PUBLISHER       "GtkGLExt"
!define GTKGLEXTMM_URL_INFO_ABOUT  "http://gtkglext.sourceforge.net/"
!define GTKGLEXTMM_URL_UPDATE_INFO "http://sourceforge.net/projects/gtkglext/"
!define GTKGLEXTMM_HELP_LINK       "mailto:gtkglext-users@lists.sourceforge.net"

;--------------------------------
;Configuration

OutFile gtkglextmm-win32-${GTKGLEXTMM_VERSION}.exe
SetCompressor lzma

InstType "Full (Runtime w/ Development and Examples)"
InstType "Runtime (w/o Development and Examples)"

ShowInstDetails show
ShowUninstDetails show
SetDateSave on

; the default installation drive is changed by .onInit
InstallDir C:\GtkGLExt\${GTKGLEXTMM_API_VERSION}
InstallDirRegKey HKLM SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION} "Path"

;--------------------------------
;Header Files

!include "MUI.nsh"

;--------------------------------
;Configuration

; Names
Name "gtkglextmm"
Caption "gtkglextmm ${GTKGLEXTMM_VERSION} Setup"

;--------------------------------
;Variables

Var MUI_TEMP
Var STARTMENU_FOLDER

;--------------------------------
;Interface Settings

!define MUI_ABORTWARNING

!define MUI_COMPONENTSPAGE_SMALLDESC

!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

;--------------------------------
;Pages

;Welcome Page
!define MUI_WELCOMEPAGE_TEXT "This wizard will guide you through the installation of gtkglextmm, C++ Wrapper for GtkGLExt.\r\n\r\n\r\n$_CLICK"
!insertmacro MUI_PAGE_WELCOME

;License Page
!insertmacro MUI_PAGE_LICENSE "COPYING.LIB"

;Components Page
!insertmacro MUI_PAGE_COMPONENTS

;Directory Page
!insertmacro MUI_PAGE_DIRECTORY

;Start Menu Folder Page
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "gtkglextmm ${GTKGLEXTMM_API_VERSION}"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "StartMenuFolder"
!insertmacro MUI_PAGE_STARTMENU StartMenu $STARTMENU_FOLDER

;Installation Page
!insertmacro MUI_PAGE_INSTFILES

;Finish Page
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
!insertmacro MUI_PAGE_FINISH

;Uninstall Welcome Page
!insertmacro MUI_UNPAGE_WELCOME

;Uninstall Confirm Page
!insertmacro MUI_UNPAGE_CONFIRM

;Uninstallation Page
!insertmacro MUI_UNPAGE_INSTFILES

;Uninstall Finish Page
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

!insertmacro MUI_LANGUAGE "English"
  
;--------------------------------
;Reserve Files

;!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;--------------------------------
;Installer Sections

Section "gtkglextmm Runtime" SecRuntime
  SectionIn 1 2 RO

  SetOutPath $INSTDIR\bin
  File bin\*.dll

  ; Registry
  WriteRegStr HKLM "SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION}\Runtime" "Path" "$INSTDIR"
  WriteRegStr HKLM "SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION}\Runtime" "Version" "${GTKGLEXTMM_VERSION}"

SectionEnd

Section "gtkglextmm Development" SecDevelopment
  SectionIn 1

  SetOutPath $INSTDIR\include\gtkglextmm-${GTKGLEXTMM_API_VERSION}
  File include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\*.h

  SetOutPath $INSTDIR\include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\gdkmm\gl
  File include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\gdkmm\gl\*.h

  SetOutPath $INSTDIR\include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\gdkmm\gl\private
  File include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\gdkmm\gl\private\*.h

  SetOutPath $INSTDIR\include\gtkglextmm-${GTKGLEXTMM_API_VERSION}
  File include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\*.h

  SetOutPath $INSTDIR\include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\gtkmm\gl
  File include\gtkglextmm-${GTKGLEXTMM_API_VERSION}\gtkmm\gl\*.h

  SetOutPath $INSTDIR\lib\gtkglextmm-${GTKGLEXTMM_API_VERSION}\include
  File lib\gtkglextmm-${GTKGLEXTMM_API_VERSION}\include\gdkglextmm-config.h

  SetOutPath $INSTDIR\lib\gtkglextmm-${GTKGLEXTMM_API_VERSION}\proc\m4
  File lib\gtkglextmm-${GTKGLEXTMM_API_VERSION}\proc\m4\*.m4

  SetOutPath $INSTDIR\lib
  File lib\*.a

  SetOutPath $INSTDIR\lib\pkgconfig
  File lib\pkgconfig\*.pc

  SetOutPath $INSTDIR\share\aclocal
  File share\aclocal\*.m4

  SetOutPath $INSTDIR\share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html
  File share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html\*.html
  File share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html\*.css
  File share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html\*.png
  File share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html\*.dot

  ; Update .pc files
  ; short name will cause a problem with current libtool :-<
  ;GetFullPathName /SHORT $0 $INSTDIR
  StrCpy $0 $INSTDIR
  Push $0
  Call ChangeDirSeparator
  Pop $0

  DetailPrint "Updating $INSTDIR\lib\pkgconfig\gdkglextmm-${GTKGLEXTMM_API_VERSION}.pc"
  Push $INSTDIR\lib\pkgconfig\gdkglextmm-${GTKGLEXTMM_API_VERSION}.pc
  Push $0
  Call UpdatePCFile
  DetailPrint "Done"

  DetailPrint "Updating $INSTDIR\lib\pkgconfig\gdkglextmm-win32-${GTKGLEXTMM_API_VERSION}.pc"
  Push $INSTDIR\lib\pkgconfig\gdkglextmm-win32-${GTKGLEXTMM_API_VERSION}.pc
  Push $0
  Call UpdatePCFile
  DetailPrint "Done"

  DetailPrint "Updating $INSTDIR\lib\pkgconfig\gtkglextmm-${GTKGLEXTMM_API_VERSION}.pc"
  Push $INSTDIR\lib\pkgconfig\gtkglextmm-${GTKGLEXTMM_API_VERSION}.pc
  Push $0
  Call UpdatePCFile
  DetailPrint "Done"

  DetailPrint "Updating $INSTDIR\lib\pkgconfig\gtkglextmm-win32-${GTKGLEXTMM_API_VERSION}.pc"
  Push $INSTDIR\lib\pkgconfig\gtkglextmm-win32-${GTKGLEXTMM_API_VERSION}.pc
  Push $0
  Call UpdatePCFile
  DetailPrint "Done"

  ; Registry
  WriteRegStr HKLM "SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION}\Development" "Path" "$INSTDIR"
  WriteRegStr HKLM "SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION}\Development" "Version" "${GTKGLEXTMM_VERSION}"

SectionEnd

Section "gtkglextmm Examples" SecExamples
  SectionIn 1

  SetOutPath $INSTDIR\gtkglextmm-examples
  File gtkglextmm-examples\Makefile.mingw
  File gtkglextmm-examples\*.h
  File gtkglextmm-examples\*.cc
  File gtkglextmm-examples\*.c
  File gtkglextmm-examples\*.exe

SectionEnd

Section "Start Menu" SecStartMenu
  SectionIn 1 2

  SetShellVarContext all

  !insertmacro MUI_STARTMENU_WRITE_BEGIN StartMenu

    CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"

    WriteINIStr "$SMPROGRAMS\$STARTMENU_FOLDER\GtkGLExt Website.url" "InternetShortcut" "URL" "http://gtkglext.sourceforge.net/"

    IfFileExists "$INSTDIR\share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html\index.html" "" +2
      CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\gtkglextmm Documentation.lnk" "$INSTDIR\share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}\html\index.html"

    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall gtkglextmm.lnk" "$INSTDIR\uninst-gtkglextmm.exe"

  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

Section -post

  ; Write $INSTDIR\bin\gtkglextmm-env.sh
  DetailPrint "Generating $INSTDIR\bin\gtkglextmm-env.sh"
  Push $INSTDIR\bin\gtkglextmm-env.sh
  Call WriteEnvSh
  DetailPrint "Done"

  ; Registry
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "DisplayName" "gtkglextmm ${GTKGLEXTMM_VERSION}"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "DisplayVersion" "${GTKGLEXTMM_VERSION}"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "Publisher" "${GTKGLEXTMM_PUBLISHER}"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "URLInfoAbout" "${GTKGLEXTMM_URL_INFO_ABOUT}"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "URLUpdateInfo" "${GTKGLEXTMM_URL_UPDATE_INFO}"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "HelpLink" "${GTKGLEXTMM_HELP_LINK}"
  WriteRegExpandStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm" "UninstallString" "$INSTDIR\uninst-gtkglextmm.exe"

  ;; Uninstaller
  Delete $INSTDIR\uninst-gtkglextmm.exe
  WriteUninstaller $INSTDIR\uninst-gtkglextmm.exe

SectionEnd

;--------------------------------
;Descriptions

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SecRuntime} "gtkglextmm runtime libraries"
  !insertmacro MUI_DESCRIPTION_TEXT ${SecDevelopment} "gtkglextmm development headers, libraries, and documentations"
  !insertmacro MUI_DESCRIPTION_TEXT ${SecExamples} "gtkglextmm example programs"
  !insertmacro MUI_DESCRIPTION_TEXT ${SecStartMenu} "Adds icons to your start menu for easy access"
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Installer Functions

Function .onInit
  ; Change the default installation drive
  StrLen $R0 $INSTDIR
  StrCpy $R1 $INSTDIR $R0 1
  StrCpy $INSTDIR $PROGRAMFILES 1
  StrCpy $INSTDIR "$INSTDIR$R1"
FunctionEnd

; ChangeDirSeparator
; input, top of stack  (e.g. C:\Program Files\...)
; output, top of stack (replaces, with e.g. C:/Program Files/...)
; modifies no other variables.
Function ChangeDirSeparator
  Exch $R0
  Push $R1
  Push $R2
  Push $R3
    StrCpy $R1 $R0
    StrCpy $R0 ""
    StrCpy $R2 0
    goto loop
    replace:
      StrCpy $R0 "$R0/"
    loop:
      StrCpy $R3 $R1 1 $R2
      IntOp $R2 $R2 + 1
      StrCmp $R3 "\" replace
      StrCmp $R3 "" done
      StrCpy $R0 "$R0$R3"
      goto loop
    done:
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
FunctionEnd

; UpdatePCFile
; update pkg-config .pc file
Function UpdatePCFile
  Pop $R0 ; prefix
  Pop $R1 ; target .pc file
  Push $R7
  Push $R8
  Push $R9
    Rename $R1 $R1.orig
    FileOpen $R7 $R1.orig r
    FileOpen $R8 $R1 w
    FileRead $R7 $R9 ; skip original prefix
    FileWrite $R8 "prefix=$R0$\n"
    loop:
      FileRead $R7 $R9
      StrCmp $R9 "" done
      FileWrite $R8 $R9
      goto loop
    done:
    FileClose $R7
    FileClose $R8
    Delete $R1.orig
  Pop $R9
  Pop $R8
  Pop $R7
FunctionEnd

; WriteEnvSh
Function WriteEnvSh
  Pop $R0 ; Output file
  Push $R8
  Push $R9
    GetFullPathName /SHORT $R8 $INSTDIR
    Push $R8
    Call ChangeDirSeparator
    Pop $R8
    FileOpen $R9 $R0 w
    FileWrite $R9 "#!/bin/sh$\n"
    FileWrite $R9 "$\n"
    FileWrite $R9 "export GTKGLEXTMM_PREFIX=$R8$\n"
    FileWrite $R9 "$\n"
    FileWrite $R9 "echo $\"Setting environment variables for gtkglextmm.$\"$\n"
    FileWrite $R9 "echo $\"$\"$\n"
    FileWrite $R9 "$\n"
    FileWrite $R9 "case $$TERM in$\n"
    FileWrite $R9 "cygwin)$\n"
    FileWrite $R9 "  GTKGLEXTMM_PATH=`cygpath -u $${GTKGLEXTMM_PREFIX}/bin`$\n"
    FileWrite $R9 "  ;;$\n"
    FileWrite $R9 "msys)$\n"
    FileWrite $R9 "  GTKGLEXTMM_PATH=/$${GTKGLEXTMM_PREFIX%:*}$${GTKGLEXTMM_PREFIX#*:}/bin$\n"
    FileWrite $R9 "  ;;$\n"
    FileWrite $R9 "*)$\n"
    FileWrite $R9 "  echo $\"Unknown TERM$\"$\n"
    FileWrite $R9 "  ;;$\n"
    FileWrite $R9 "esac$\n"
    FileWrite $R9 "$\n"
    FileWrite $R9 "echo $\"export PATH=\$\"$${GTKGLEXTMM_PATH}:\$${PATH}\$\"$\"$\n"
    FileWrite $R9 "export PATH=$\"$${GTKGLEXTMM_PATH}:$${PATH}$\"$\n"
    FileWrite $R9 "$\n"
    FileWrite $R9 "echo $\"export PKG_CONFIG_PATH=\$\"$${GTKGLEXTMM_PREFIX}/lib/pkgconfig;\$${PKG_CONFIG_PATH}\$\"$\"$\n"
    FileWrite $R9 "export PKG_CONFIG_PATH=$\"$${GTKGLEXTMM_PREFIX}/lib/pkgconfig;$${PKG_CONFIG_PATH}$\"$\n"
    FileWrite $R9 "$\n"
    FileWrite $R9 "echo $\"$\"$\n"
    FileClose $R9
  Pop $R9
  Pop $R8
FunctionEnd

;--------------------------------
;Uninstaller Section

Section Uninstall

  ;Remove shortcut
  SetShellVarContext all
  !insertmacro MUI_STARTMENU_GETFOLDER StartMenu $MUI_TEMP
  StrCmp $MUI_TEMP "" noshortcuts
    Delete "$SMPROGRAMS\$MUI_TEMP\GtkGLExt Website.url"
    Delete "$SMPROGRAMS\$MUI_TEMP\gtkglextmm Documentation.lnk"
    Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall gtkglextmm.lnk"
    RMDir "$SMPROGRAMS\$MUI_TEMP" ;Only if empty, so it won't delete other shortcuts
  noshortcuts:

  DeleteRegKey HKLM "SOFTWARE\gtkglextmm\${GTKGLEXTMM_API_VERSION}"
  DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\gtkglextmm"

  Delete $INSTDIR\uninst-gtkglextmm.exe

  ; bin

  Delete $INSTDIR\bin\gtkglextmm-env.sh
  Delete $INSTDIR\bin\libgdkglextmm-win32-${GTKGLEXTMM_API_VERSION}-*.dll
  Delete $INSTDIR\bin\libgtkglextmm-win32-${GTKGLEXTMM_API_VERSION}-*.dll

  RMDir $INSTDIR\bin

  ; include

  RMDir /r $INSTDIR\include\gtkglextmm-1.0

  RMDir $INSTDIR\include

  ; lib

  RMDir /r $INSTDIR\lib\gtkglextmm-1.0

  Delete $INSTDIR\lib\libgdkglextmm-win32-${GTKGLEXTMM_API_VERSION}.dll.a
  Delete $INSTDIR\lib\libgtkglextmm-win32-${GTKGLEXTMM_API_VERSION}.dll.a

  Delete $INSTDIR\lib\pkgconfig\gdkglextmm-${GTKGLEXTMM_API_VERSION}.pc
  Delete $INSTDIR\lib\pkgconfig\gdkglextmm-win32-${GTKGLEXTMM_API_VERSION}.pc
  Delete $INSTDIR\lib\pkgconfig\gtkglextmm-${GTKGLEXTMM_API_VERSION}.pc
  Delete $INSTDIR\lib\pkgconfig\gtkglextmm-win32-${GTKGLEXTMM_API_VERSION}.pc
  RMDir $INSTDIR\lib\pkgconfig

  RMDir $INSTDIR\lib

  ; share

  Delete $INSTDIR\share\aclocal\gtkglextmm-${GTKGLEXTMM_API_VERSION}.m4
  RMDir $INSTDIR\share\aclocal

  RMDir /r $INSTDIR\share\doc\gtkglextmm-${GTKGLEXTMM_API_VERSION}
  RMDir $INSTDIR\share\doc

  RMDir $INSTDIR\share

  ; gtkglextmm-examples

  RMDir /r $INSTDIR\gtkglextmm-examples

  ; $INSTDIR

  RMDir $INSTDIR

SectionEnd
