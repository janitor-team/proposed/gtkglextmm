# -*- rpm-spec -*-

%define base_version		1.2.0
%define api_version		1.2
%define rel			1

%define gtkglext_req_version	1.0.0

Summary: C++ Wrapper for GtkGLExt
Name: gtkglextmm
Version: %{base_version}
Release: %{rel}
License: LGPL
Group: System Environment/Libraries
URL: http://gtkglext.sourceforge.net/
Source0: ftp://dl.sourceforge.net/pub/sourceforge/gtkglext/gtkglextmm-%{version}.tar.gz
BuildRoot: %{_tmppath}/gtkglextmm-%{version}-root

Requires: gtkglext >= %{gtkglext_req_version}
Requires: gtkmm2
Requires: gtk2
Requires: XFree86-libs

BuildRequires: gtkglext-devel >= %{gtkglext_req_version}
BuildRequires: gtkmm2-devel
BuildRequires: gtk2-devel
BuildRequires: XFree86-devel
BuildRequires: pkgconfig

%description
gtkglextmm is C++ wrapper for GtkGLExt, OpenGL Extension to GTK.
It enables C++ programmers to write OpenGL applications with gtkmm2.

%package devel
Summary: Development tools for GTK-based OpenGL applications
Group: Development/Libraries

Requires: %{name} = %{version}
Requires: gtkglext-devel >= %{gtkglext_req_version}
Requires: gtkmm2-devel
Requires: gtk2-devel
Requires: XFree86-devel

%description devel
The gtkglextmm-devel package contains the header files, static libraries,
and developer docs for gtkglextmm.

%prep
%setup -q -n gtkglextmm-%{version}

%build
%configure --disable-doxygen-doc
make

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

pushd "$RPM_BUILD_ROOT%{_libdir}"

cp -p libgdkglextmm-x11-%{api_version}.la libgdkglextmm-x11-%{api_version}.la.bak
cat libgdkglextmm-x11-%{api_version}.la.bak | \
  sed -e "s| -L$RPM_BUILD_ROOT%{_libdir}||g" > libgdkglextmm-x11-%{api_version}.la
rm -f libgdkglextmm-x11-%{api_version}.la.bak

cp -p libgtkglextmm-x11-%{api_version}.la libgtkglextmm-x11-%{api_version}.la.bak
cat libgtkglextmm-x11-%{api_version}.la.bak | \
  sed -e "s| -L$RPM_BUILD_ROOT%{_libdir}||g" > libgtkglextmm-x11-%{api_version}.la
rm -f libgtkglextmm-x11-%{api_version}.la.bak

popd

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)

%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README TODO
%{_libdir}/libgdkglextmm-x11-%{api_version}.so.*
%{_libdir}/libgtkglextmm-x11-%{api_version}.so.*

%files devel
%defattr(-,root,root,-)

%{_includedir}/*
%{_libdir}/gtkglextmm-%{api_version}
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/*
%{_datadir}/doc/*

%changelog
* Sun Aug 31 2003 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Updated source URL.

* Sun May 11 2003 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Removed LDFLAGS setting.
- Removed atk, pango, glib2 from Requires.
- Remove lib*.la.bak files.

* Mon Feb 24 2003 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Added %{_datadir}/aclocal/* to the file list.

* Tue Dec 17 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Changed documentation directory.

* Mon Dec 16 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Added %{_libdir}/gtkglextmm-%{api_version} to the file list.

* Thu Aug  8 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Documentation have been added.

* Mon Jul 22 2002 Naofumi Yasufuku <naofumi@users.sourceforge.net>
- Initial build.


