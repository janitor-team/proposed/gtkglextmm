<root>
<function name="gtk_widget_get_gl_config">
<description>
Returns the #GdkGLConfig referred by the @widget.


</description>
<parameters>
<parameter name="widget">
<parameter_description> a #GtkWidget.
</parameter_description>
</parameter>
</parameters>
<return> the #GdkGLConfig.
</return>
</function>

<function name="gtk_widget_get_gl_window">
<description>
Returns the #GdkGLWindow owned by the @widget.


</description>
<parameters>
<parameter name="widget">
<parameter_description> a #GtkWidget.
</parameter_description>
</parameter>
</parameters>
<return> the #GdkGLWindow.
</return>
</function>

<function name="gtk_gl_init_check">
<description>
This function does the same work as gtk_gl_init() with only 
a single change: It does not terminate the program if the library can&apos;t be 
initialized. Instead it returns %FALSE on failure.

This way the application can fall back to some other means of communication 
with the user - for example a curses or command line interface.


</description>
<parameters>
<parameter name="argc">
<parameter_description> Address of the &amp;lt;parameter&amp;gt;argc&amp;lt;/parameter&amp;gt; parameter of your 
&amp;lt;function&amp;gt;main()&amp;lt;/function&amp;gt; function. Changed if any arguments
were handled.
</parameter_description>
</parameter>
<parameter name="argv">
<parameter_description> Address of the &amp;lt;parameter&amp;gt;argv&amp;lt;/parameter&amp;gt; parameter of 
&amp;lt;function&amp;gt;main()&amp;lt;/function&amp;gt;. Any parameters understood by
gtk_gl_init() are stripped before return.
</parameter_description>
</parameter>
</parameters>
<return> %TRUE if the GUI has been successfully initialized, 
%FALSE otherwise.
</return>
</function>

<function name="gtk_gl_init">
<description>
Call this function before using any other GtkGLExt functions in your 
applications.  It will initialize everything needed to operate the library
and parses some standard command line options. @argc and 
@argv are adjusted accordingly so your own code will 
never see those standard arguments.

&amp;lt;note&amp;gt;&amp;lt;para&amp;gt;
This function will terminate your program if it was unable to initialize 
the library for some reason. If you want your program to fall back to a 
textual interface you want to call gtk_gl_init_check() instead.
&amp;lt;/para&amp;gt;&amp;lt;/note&amp;gt;

</description>
<parameters>
<parameter name="argc">
<parameter_description> Address of the &amp;lt;parameter&amp;gt;argc&amp;lt;/parameter&amp;gt; parameter of your 
&amp;lt;function&amp;gt;main()&amp;lt;/function&amp;gt; function. Changed if any arguments
were handled.
</parameter_description>
</parameter>
<parameter name="argv">
<parameter_description> Address of the &amp;lt;parameter&amp;gt;argv&amp;lt;/parameter&amp;gt; parameter of 
&amp;lt;function&amp;gt;main()&amp;lt;/function&amp;gt;. Any parameters understood by
gtk_gl_init() are stripped before return.
</parameter_description>
</parameter>
</parameters>
<return></return>
</function>

<function name="gtk_widget_is_gl_capable">
<description>
Returns whether the @widget is OpenGL-capable.


</description>
<parameters>
<parameter name="widget">
<parameter_description> a #GtkWidget.
</parameter_description>
</parameter>
</parameters>
<return> TRUE if the @widget is OpenGL-capable, FALSE otherwise.
</return>
</function>

<function name="gtk_widget_set_gl_capability">
<description>
Set the OpenGL-capability to the @widget.
This function prepares the widget for its use with OpenGL.


</description>
<parameters>
<parameter name="widget">
<parameter_description> the #GtkWidget to be used as the rendering area.
</parameter_description>
</parameter>
<parameter name="glconfig">
<parameter_description> a #GdkGLConfig.
</parameter_description>
</parameter>
<parameter name="share_list">
<parameter_description> the #GdkGLContext with which to share display lists and texture
objects. NULL indicates that no sharing is to take place.
</parameter_description>
</parameter>
<parameter name="direct">
<parameter_description> whether rendering is to be done with a direct connection to
the graphics system.
</parameter_description>
</parameter>
<parameter name="render_type">
<parameter_description> GDK_GL_RGBA_TYPE or GDK_GL_COLOR_INDEX_TYPE (currently not
used).
</parameter_description>
</parameter>
</parameters>
<return> TRUE if it is successful, FALSE otherwise.
</return>
</function>

<function name="gtk_widget_get_gl_context">
<description>
Returns the #GdkGLContext with the appropriate #GdkGLDrawable
for this widget. Unlike the GL context returned by
gtk_widget_create_gl_context(),  this context is owned by the widget.

#GdkGLContext is needed for the function gdk_gl_drawable_begin,
or for sharing display lists (see gtk_widget_set_gl_capability()).


</description>
<parameters>
<parameter name="widget">
<parameter_description> a #GtkWidget.
</parameter_description>
</parameter>
</parameters>
<return> the #GdkGLContext.
</return>
</function>

<function name="gtk_gl_parse_args">
<description>
Parses command line arguments, and initializes global
attributes of GtkGLExt.

Any arguments used by GtkGLExt are removed from the array and
@argc and @argv are updated accordingly.

You shouldn&apos;t call this function explicitely if you are using
gtk_gl_init(), or gtk_gl_init_check().


</description>
<parameters>
<parameter name="argc">
<parameter_description> the number of command line arguments.
</parameter_description>
</parameter>
<parameter name="argv">
<parameter_description> the array of command line arguments.
</parameter_description>
</parameter>
</parameters>
<return> %TRUE if initialization succeeded, otherwise %FALSE.
</return>
</function>

<function name="gtk_widget_create_gl_context">
<description>
Creates a new #GdkGLContext with the appropriate #GdkGLDrawable
for this widget. The GL context must be freed when you&apos;re
finished with it. See also gtk_widget_get_gl_context().


</description>
<parameters>
<parameter name="widget">
<parameter_description> a #GtkWidget.
</parameter_description>
</parameter>
<parameter name="share_list">
<parameter_description> the #GdkGLContext with which to share display lists and texture
objects. NULL indicates that no sharing is to take place.
</parameter_description>
</parameter>
<parameter name="direct">
<parameter_description> whether rendering is to be done with a direct connection to
the graphics system.
</parameter_description>
</parameter>
<parameter name="render_type">
<parameter_description> GDK_GL_RGBA_TYPE or GDK_GL_COLOR_INDEX_TYPE (currently not
used).
</parameter_description>
</parameter>
</parameters>
<return> the new #GdkGLContext.
</return>
</function>

</root>
