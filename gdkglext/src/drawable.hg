// -*- C++ -*-
/* gdkglextmm - C++ Wrapper for GdkGLExt
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#include <glibmm/interface.h>

#include <gdkmm/gl/defs.h>
#include <gdkmm/gl/config.h>
#include <gdkmm/gl/context.h>

#include <GL/gl.h>

_DEFS(gdkmm/gl,gdkglext)
_PINCLUDE(glibmm/private/interface_p.h)

namespace Gdk
{
namespace GL
{

class Context;

  /** @defgroup GLDrawables  GL Drawables
   */

  /** OpenGL rendering surface abstract class.
   *
   *
   */

class Drawable : public Glib::Interface
{
  _CLASS_INTERFACE(Drawable, GdkGLDrawable, GDK_GL_DRAWABLE, GdkGLDrawableClass)

public:

  _WRAP_METHOD(bool make_current(const Glib::RefPtr<Context>& glcontext), gdk_gl_drawable_make_current)

  _WRAP_METHOD(bool is_double_buffered() const, gdk_gl_drawable_is_double_buffered)

  _WRAP_METHOD(void swap_buffers(), gdk_gl_drawable_swap_buffers)

  _WRAP_METHOD(void wait_gl(), gdk_gl_drawable_wait_gl)

  _WRAP_METHOD(void wait_gdk(), gdk_gl_drawable_wait_gdk)

  _WRAP_METHOD(bool gl_begin(const Glib::RefPtr<Context>& glcontext), gdk_gl_drawable_gl_begin)

  _WRAP_METHOD(void gl_end(), gdk_gl_drawable_gl_end)

  _WRAP_METHOD(Glib::RefPtr<Config> get_gl_config(), gdk_gl_drawable_get_gl_config, refreturn)
  _WRAP_METHOD(Glib::RefPtr<const Config> get_gl_config() const, gdk_gl_drawable_get_gl_config, refreturn)

  _WRAP_METHOD(void get_size(int& width, int& height), gdk_gl_drawable_get_size)

  _WRAP_METHOD(static Glib::RefPtr<Drawable> get_current(), gdk_gl_drawable_get_current, refreturn)

public:

  _WRAP_METHOD(static void draw_cube(bool solid, double size), gdk_gl_draw_cube)

  _WRAP_METHOD(static void draw_sphere(bool solid, double radius, int slices, int stacks), gdk_gl_draw_sphere)

  _WRAP_METHOD(static void draw_cone(bool solid, double base, double height, int slices, int stacks), gdk_gl_draw_cone)

  _WRAP_METHOD(static void draw_torus(bool solid, double inner_radius, double outer_radius, int nsides, int rings), gdk_gl_draw_torus)

  _WRAP_METHOD(static void draw_tetrahedron(bool solid), gdk_gl_draw_tetrahedron)

  _WRAP_METHOD(static void draw_octahedron(bool solid), gdk_gl_draw_octahedron)

  _WRAP_METHOD(static void draw_dodecahedron(bool solid), gdk_gl_draw_dodecahedron)

  _WRAP_METHOD(static void draw_icosahedron(bool solid), gdk_gl_draw_icosahedron)

  _WRAP_METHOD(static void draw_teapot(bool solid, double scale), gdk_gl_draw_teapot)

};

  /** @example shapes.h
   *
   * Geometric object rendering example.
   *
   */

  /** @example shapes.cc
   *
   * Geometric object rendering example.
   *
   */

} // namespace GL
} // namespace Gdk
