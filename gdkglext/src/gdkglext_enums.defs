;; -*- scheme -*-

;; From gtkglext/gdk/gdkglconfig.h

(define-flags-extended ConfigMode
  (in-module "Gdk")
  (c-name "GdkGLConfigMode")
  (values
    '("rgb" "GDK_GL_MODE_RGB" "0")
    '("rgba" "GDK_GL_MODE_RGBA" "0")
    '("index" "GDK_GL_MODE_INDEX" "1 << 0")
    '("single" "GDK_GL_MODE_SINGLE" "0")
    '("double" "GDK_GL_MODE_DOUBLE" "1 << 1")
    '("stereo" "GDK_GL_MODE_STEREO" "1 << 2")
    '("alpha" "GDK_GL_MODE_ALPHA" "1 << 3")
    '("depth" "GDK_GL_MODE_DEPTH" "1 << 4")
    '("stencil" "GDK_GL_MODE_STENCIL" "1 << 5")
    '("accum" "GDK_GL_MODE_ACCUM" "1 << 6")
    '("multisample" "GDK_GL_MODE_MULTISAMPLE" "1 << 7")
  )
)

;; From gtkglext/gdk/gdkgldebug.h

(define-flags-extended DebugFlag
  (in-module "Gdk")
  (c-name "GdkGLDebugFlag")
  (values
    '("misc" "GDK_GL_DEBUG_MISC" "1 << 0")
    '("func" "GDK_GL_DEBUG_FUNC" "1 << 1")
    '("impl" "GDK_GL_DEBUG_IMPL" "1 << 2")
  )
)

;; From gtkglext/gdk/gdkgltokens.h

(define-enum-extended ConfigAttrib
  (in-module "Gdk")
  (c-name "GdkGLConfigAttrib")
  (values
    '("use-gl" "GDK_GL_USE_GL" "1")
    '("buffer-size" "GDK_GL_BUFFER_SIZE" "2")
    '("level" "GDK_GL_LEVEL" "3")
    '("rgba" "GDK_GL_RGBA" "4")
    '("doublebuffer" "GDK_GL_DOUBLEBUFFER" "5")
    '("stereo" "GDK_GL_STEREO" "6")
    '("aux-buffers" "GDK_GL_AUX_BUFFERS" "7")
    '("red-size" "GDK_GL_RED_SIZE" "8")
    '("green-size" "GDK_GL_GREEN_SIZE" "9")
    '("blue-size" "GDK_GL_BLUE_SIZE" "10")
    '("alpha-size" "GDK_GL_ALPHA_SIZE" "11")
    '("depth-size" "GDK_GL_DEPTH_SIZE" "12")
    '("stencil-size" "GDK_GL_STENCIL_SIZE" "13")
    '("accum-red-size" "GDK_GL_ACCUM_RED_SIZE" "14")
    '("accum-green-size" "GDK_GL_ACCUM_GREEN_SIZE" "15")
    '("accum-blue-size" "GDK_GL_ACCUM_BLUE_SIZE" "16")
    '("accum-alpha-size" "GDK_GL_ACCUM_ALPHA_SIZE" "17")
    '("config-caveat" "GDK_GL_CONFIG_CAVEAT" "0x20")
    '("x-visual-type" "GDK_GL_X_VISUAL_TYPE" "0x22")
    '("transparent-type" "GDK_GL_TRANSPARENT_TYPE" "0x23")
    '("transparent-index-value" "GDK_GL_TRANSPARENT_INDEX_VALUE" "0x24")
    '("transparent-red-value" "GDK_GL_TRANSPARENT_RED_VALUE" "0x25")
    '("transparent-green-value" "GDK_GL_TRANSPARENT_GREEN_VALUE" "0x26")
    '("transparent-blue-value" "GDK_GL_TRANSPARENT_BLUE_VALUE" "0x27")
    '("transparent-alpha-value" "GDK_GL_TRANSPARENT_ALPHA_VALUE" "0x28")
    '("drawable-type" "GDK_GL_DRAWABLE_TYPE" "0x8010")
    '("render-type" "GDK_GL_RENDER_TYPE" "0x8011")
    '("x-renderable" "GDK_GL_X_RENDERABLE" "0x8012")
    '("fbconfig-id" "GDK_GL_FBCONFIG_ID" "0x8013")
    '("max-pbuffer-width" "GDK_GL_MAX_PBUFFER_WIDTH" "0x8016")
    '("max-pbuffer-height" "GDK_GL_MAX_PBUFFER_HEIGHT" "0x8017")
    '("max-pbuffer-pixels" "GDK_GL_MAX_PBUFFER_PIXELS" "0x8018")
    '("visual-id" "GDK_GL_VISUAL_ID" "0x800B")
    '("screen" "GDK_GL_SCREEN" "0x800C")
    '("sample-buffers" "GDK_GL_SAMPLE_BUFFERS" "100000")
    '("samples" "GDK_GL_SAMPLES" "100001")
  )
)

(define-enum-extended ConfigCaveat
  (in-module "Gdk")
  (c-name "GdkGLConfigCaveat")
  (values
    '("config-caveat-dont-care" "GDK_GL_CONFIG_CAVEAT_DONT_CARE" "0xFFFFFFFF")
    '("config-caveat-none" "GDK_GL_CONFIG_CAVEAT_NONE" "0x8000")
    '("slow-config" "GDK_GL_SLOW_CONFIG" "0x8001")
    '("non-conformant-config" "GDK_GL_NON_CONFORMANT_CONFIG" "0x800D")
  )
)

(define-enum-extended VisualType
  (in-module "Gdk")
  (c-name "GdkGLVisualType")
  (values
    '("visual-type-dont-care" "GDK_GL_VISUAL_TYPE_DONT_CARE" "0xFFFFFFFF")
    '("true-color" "GDK_GL_TRUE_COLOR" "0x8002")
    '("direct-color" "GDK_GL_DIRECT_COLOR" "0x8003")
    '("pseudo-color" "GDK_GL_PSEUDO_COLOR" "0x8004")
    '("static-color" "GDK_GL_STATIC_COLOR" "0x8005")
    '("gray-scale" "GDK_GL_GRAY_SCALE" "0x8006")
    '("static-gray" "GDK_GL_STATIC_GRAY" "0x8007")
  )
)

(define-enum-extended TransparentType
  (in-module "Gdk")
  (c-name "GdkGLTransparentType")
  (values
    '("none" "GDK_GL_TRANSPARENT_NONE" "0x8000")
    '("rgb" "GDK_GL_TRANSPARENT_RGB" "0x8008")
    '("index" "GDK_GL_TRANSPARENT_INDEX" "0x8009")
  )
)

(define-flags-extended DrawableTypeMask
  (in-module "Gdk")
  (c-name "GdkGLDrawableTypeMask")
  (values
    '("window-bit" "GDK_GL_WINDOW_BIT" "1 << 0")
    '("pixmap-bit" "GDK_GL_PIXMAP_BIT" "1 << 1")
    '("pbuffer-bit" "GDK_GL_PBUFFER_BIT" "1 << 2")
  )
)

(define-flags-extended RenderTypeMask
  (in-module "Gdk")
  (c-name "GdkGLRenderTypeMask")
  (values
    '("rgba-bit" "GDK_GL_RGBA_BIT" "1 << 0")
    '("color-index-bit" "GDK_GL_COLOR_INDEX_BIT" "1 << 1")
  )
)

(define-flags-extended BufferMask
  (in-module "Gdk")
  (c-name "GdkGLBufferMask")
  (values
    '("front-left-buffer-bit" "GDK_GL_FRONT_LEFT_BUFFER_BIT" "1 << 0")
    '("front-right-buffer-bit" "GDK_GL_FRONT_RIGHT_BUFFER_BIT" "1 << 1")
    '("back-left-buffer-bit" "GDK_GL_BACK_LEFT_BUFFER_BIT" "1 << 2")
    '("back-right-buffer-bit" "GDK_GL_BACK_RIGHT_BUFFER_BIT" "1 << 3")
    '("aux-buffers-bit" "GDK_GL_AUX_BUFFERS_BIT" "1 << 4")
    '("depth-buffer-bit" "GDK_GL_DEPTH_BUFFER_BIT" "1 << 5")
    '("stencil-buffer-bit" "GDK_GL_STENCIL_BUFFER_BIT" "1 << 6")
    '("accum-buffer-bit" "GDK_GL_ACCUM_BUFFER_BIT" "1 << 7")
  )
)

(define-enum-extended ConfigError
  (in-module "Gdk")
  (c-name "GdkGLConfigError")
  (values
    '("bad-screen" "GDK_GL_BAD_SCREEN" "1")
    '("bad-attribute" "GDK_GL_BAD_ATTRIBUTE" "2")
    '("no-extension" "GDK_GL_NO_EXTENSION" "3")
    '("bad-visual" "GDK_GL_BAD_VISUAL" "4")
    '("bad-context" "GDK_GL_BAD_CONTEXT" "5")
    '("bad-value" "GDK_GL_BAD_VALUE" "6")
    '("bad-enum" "GDK_GL_BAD_ENUM" "7")
  )
)

(define-enum-extended RenderType
  (in-module "Gdk")
  (c-name "GdkGLRenderType")
  (values
    '("rgba-type" "GDK_GL_RGBA_TYPE" "0x8014")
    '("color-index-type" "GDK_GL_COLOR_INDEX_TYPE" "0x8015")
  )
)

(define-enum-extended DrawableAttrib
  (in-module "Gdk")
  (c-name "GdkGLDrawableAttrib")
  (values
    '("preserved-contents" "GDK_GL_PRESERVED_CONTENTS" "0x801B")
    '("largest-pbuffer" "GDK_GL_LARGEST_PBUFFER" "0x801C")
    '("width" "GDK_GL_WIDTH" "0x801D")
    '("height" "GDK_GL_HEIGHT" "0x801E")
    '("event-mask" "GDK_GL_EVENT_MASK" "0x801F")
  )
)

(define-enum-extended PbufferAttrib
  (in-module "Gdk")
  (c-name "GdkGLPbufferAttrib")
  (values
    '("preserved-contents" "GDK_GL_PBUFFER_PRESERVED_CONTENTS" "0x801B")
    '("largest-pbuffer" "GDK_GL_PBUFFER_LARGEST_PBUFFER" "0x801C")
    '("height" "GDK_GL_PBUFFER_HEIGHT" "0x8040")
    '("width" "GDK_GL_PBUFFER_WIDTH" "0x8041")
  )
)

(define-flags-extended EventMask
  (in-module "Gdk")
  (c-name "GdkGLEventMask")
  (values
    '("k" "GDK_GL_PBUFFER_CLOBBER_MASK" "1 << 27")
  )
)

(define-enum-extended EventType
  (in-module "Gdk")
  (c-name "GdkGLEventType")
  (values
    '("damaged" "GDK_GL_DAMAGED" "0x8020")
    '("saved" "GDK_GL_SAVED" "0x8021")
  )
)

(define-enum-extended DrawableType
  (in-module "Gdk")
  (c-name "GdkGLDrawableType")
  (values
    '("window" "GDK_GL_WINDOW" "0x8022")
    '("pbuffer" "GDK_GL_PBUFFER" "0x8023")
  )
)

