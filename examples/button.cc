// -*- C++ -*-
/*
 * button.cc:
 * Simple toggle button example.
 *
 * written by Naofumi Yasufuku  <naofumi@users.sourceforge.net>
 */

#include <iostream>
#include <cstdlib>
#include <cmath>

#include <gtkmm.h>

#include <gtkglmm.h>

#ifdef G_OS_WIN32
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>


///////////////////////////////////////////////////////////////////////////////
//
// OpenGL scene.
//
///////////////////////////////////////////////////////////////////////////////

class GLScene : public Gtk::GL::DrawingArea
{
public:
  static const unsigned int TIMEOUT_INTERVAL;

public:
  GLScene(const Glib::RefPtr<const Gdk::GL::Config>& config);
  virtual ~GLScene();

protected:
  virtual void on_realize();
  virtual bool on_configure_event(GdkEventConfigure* event);
  virtual bool on_expose_event(GdkEventExpose* event);
  virtual bool on_map_event(GdkEventAny* event);
  virtual bool on_unmap_event(GdkEventAny* event);
  virtual bool on_visibility_notify_event(GdkEventVisibility* event);
  virtual bool on_timeout();

public:
  // Invalidate whole window.
  void invalidate() {
    get_window()->invalidate_rect(get_allocation(), false);
  }

  // Update window synchronously (fast).
  void update()
  { get_window()->process_updates(false); }

protected:
  void timeout_add();
  void timeout_remove();

public:
  void toggle_animation();

protected:
  bool m_Animate;
  // timeout signal connection:
  sigc::connection m_ConnectionTimeout;

protected:
  GLfloat m_Angle;
  GLfloat m_PosY;

};

const unsigned int GLScene::TIMEOUT_INTERVAL = 10;

GLScene::GLScene(const Glib::RefPtr<const Gdk::GL::Config>& config)
  : Gtk::GL::DrawingArea(config),
    m_Animate(true), m_Angle(0.0), m_PosY(0.0)
{
  set_size_request(200, 200);
  add_events(Gdk::VISIBILITY_NOTIFY_MASK);
}

GLScene::~GLScene()
{
}

void GLScene::on_realize()
{
  // We need to call the base on_realize()
  Gtk::GL::DrawingArea::on_realize();

  //
  // Get GL::Window.
  //

  Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

  //
  // GL calls.
  //

  // *** OpenGL BEGIN ***
  if (!glwindow->gl_begin(get_gl_context()))
    return;

  static GLfloat ambient[]  = { 0.0, 0.0, 0.0, 1.0 };
  static GLfloat diffuse[]  = { 1.0, 1.0, 1.0, 1.0 };
  static GLfloat position[] = { 1.0, 1.0, 1.0, 0.0 };
  static GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
  static GLfloat local_view[] = {0.0};

  glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
  glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
  glLightfv (GL_LIGHT0, GL_POSITION, position);
  glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
  glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT0);
  glEnable (GL_DEPTH_TEST);

  glClearColor (1.0, 1.0, 1.0, 1.0);
  glClearDepth (1.0);

  glwindow->gl_end();
  // *** OpenGL END ***
}

bool GLScene::on_configure_event(GdkEventConfigure* event)
{
  //
  // Get GL::Window.
  //

  Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

  //
  // GL calls.
  //

  // *** OpenGL BEGIN ***
  if (!glwindow->gl_begin(get_gl_context()))
    return false;

  GLfloat w = get_width();
  GLfloat h = get_height();
  GLfloat aspect;

  glViewport(0, 0, static_cast<GLsizei>(w), static_cast<GLsizei>(h));

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (w > h)
    {
      aspect = w / h;
      glFrustum(-aspect, aspect, -1.0, 1.0, 5.0, 60.0);
    }
  else
    {
      aspect = h / w;
      glFrustum(-1.0, 1.0, -aspect, aspect, 5.0, 60.0);
    }

  glMatrixMode(GL_MODELVIEW);

  glwindow->gl_end();
  // *** OpenGL END ***

  return true;
}

bool GLScene::on_expose_event(GdkEventExpose* event)
{
  //
  // Get GL::Window.
  //

  Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

  //
  // GL calls.
  //

  // *** OpenGL BEGIN ***
  if (!glwindow->gl_begin(get_gl_context()))
    return false;

  // brass
  static GLfloat ambient[4]  = { 0.329412, 0.223529, 0.027451, 1.0 };
  static GLfloat diffuse[4]  = { 0.780392, 0.568627, 0.113725, 1.0 };
  static GLfloat specular[4] = { 0.992157, 0.941176, 0.807843, 1.0 };
  static GLfloat shininess   = 0.21794872 * 128.0;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glLoadIdentity();
  glTranslatef(0.0, 0.0, -10.0);

  glPushMatrix();
    glTranslatef(0.0, m_PosY, 0.0);
    glRotatef(m_Angle, 0.0, 1.0, 0.0);
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);
    glwindow->draw_torus(true, 0.3, 0.6, 30, 30);
  glPopMatrix();

  // Swap buffers.
  if (glwindow->is_double_buffered())
    glwindow->swap_buffers();
  else
    glFlush();

  glwindow->gl_end();
  // *** OpenGL END ***

  return true;
}

bool GLScene::on_timeout()
{
  m_Angle += 1.0;
  if (m_Angle >= 360.0)
    m_Angle -= 360.0;

  float t = m_Angle * G_PI / 180.0;
  if (t > G_PI)
    t = 2.0 * G_PI - t;

  m_PosY = 2.0 * (std::sin (t) + 0.4 * std::sin (3.0*t)) - 1.0;

  // Invalidate whole window.
  invalidate();
  // Update window synchronously (fast).
  update();

  return true;
}

void GLScene::timeout_add()
{
  if (!m_ConnectionTimeout.connected())
    m_ConnectionTimeout = Glib::signal_timeout().connect(
      sigc::mem_fun(*this, &GLScene::on_timeout), TIMEOUT_INTERVAL);
}

void GLScene::timeout_remove()
{
  if (m_ConnectionTimeout.connected())
    m_ConnectionTimeout.disconnect();
}

bool GLScene::on_map_event(GdkEventAny* event)
{
  if (m_Animate)
    timeout_add();

  return true;
}

bool GLScene::on_unmap_event(GdkEventAny* event)
{
  timeout_remove();

  return true;
}

bool GLScene::on_visibility_notify_event(GdkEventVisibility* event)
{
  if (m_Animate)
    {
      if (event->state == GDK_VISIBILITY_FULLY_OBSCURED)
        timeout_remove();
      else
        timeout_add();
    }

  return true;
}

void GLScene::toggle_animation()
{
  m_Animate = !m_Animate;

  if (m_Animate)
    {
      timeout_add();
    }
  else
    {
      timeout_remove();
      invalidate();
    }
}


///////////////////////////////////////////////////////////////////////////////
//
// Toggle button which contains an OpenGL scene.
//
///////////////////////////////////////////////////////////////////////////////

class GLToggleButton : public Gtk::ToggleButton
{
public:
  GLToggleButton(const Glib::RefPtr<const Gdk::GL::Config>& config);
  virtual ~GLToggleButton();

protected:
  virtual void on_toggled();

protected:
  Gtk::VBox m_VBox;
  GLScene m_GLScene;
  Gtk::Label m_Label;

};

GLToggleButton::GLToggleButton(const Glib::RefPtr<const Gdk::GL::Config>& config)
  : m_VBox(false, 0), m_GLScene(config), m_Label("Toggle Animation")
{
  m_VBox.set_border_width(10);
  m_VBox.pack_start(m_GLScene);
  m_VBox.pack_start(m_Label, Gtk::PACK_SHRINK, 10);

  add(m_VBox);
}

GLToggleButton::~GLToggleButton()
{
}

void GLToggleButton::on_toggled()
{
  m_GLScene.toggle_animation();
}


///////////////////////////////////////////////////////////////////////////////
//
// The application class.
//
///////////////////////////////////////////////////////////////////////////////

class Button : public Gtk::Window
{
public:
  Button(const Glib::RefPtr<const Gdk::GL::Config>& config);
  virtual ~Button();

protected:
  virtual bool on_delete_event(GdkEventAny* event);

protected:
  // member widgets:
  GLToggleButton m_Button;

};

Button::Button(const Glib::RefPtr<const Gdk::GL::Config>& config)
  : m_Button(config)
{
  //
  // Top-level window.
  //

  set_title("button");

  // Get automatically redrawn if any of their children changed allocation.
  set_reallocate_redraws(true);
  // Set border width.
  set_border_width(10);

  //
  // Add toggle button.
  //

  add(m_Button);

  //
  // Show window.
  //

  show_all();
}

Button::~Button()
{
}

bool Button::on_delete_event(GdkEventAny* event)
{
  Gtk::Main::quit();

  return true;
}


///////////////////////////////////////////////////////////////////////////////
//
// Main.
//
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  Gtk::Main kit(argc, argv);

  //
  // Init gtkglextmm.
  //

  Gtk::GL::init(argc, argv);

  //
  // Configure OpenGL-capable visual.
  //

  Glib::RefPtr<Gdk::GL::Config> glconfig;

  // Try double-buffered visual
  glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB    |
                                     Gdk::GL::MODE_DEPTH  |
                                     Gdk::GL::MODE_DOUBLE);
  if (!glconfig)
    {
      std::cerr << "*** Cannot find the double-buffered visual.\n"
                << "*** Trying single-buffered visual.\n";

      // Try single-buffered visual
      glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB   |
                                         Gdk::GL::MODE_DEPTH);
      if (!glconfig)
        {
          std::cerr << "*** Cannot find any OpenGL-capable visual.\n";
          std::exit(1);
        }
    }

  //
  // Instantiate and run the application.
  //

  Button button(glconfig);

  kit.run(button);

  return 0;
}
